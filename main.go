package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"lang/interp"
	"lang/parser"
	"lang/scan"
	"log"
	"os"
)

func main() {
	var l = len(os.Args)
	if l > 2 {
		fmt.Println("Usage: lang")
	} else if l == 2 {
		runFile(os.Args[1])
	} else {
		runPrompt()
	}
}

func runFile(path string) {
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalln(err)
	}
	_, err = run(string(buf))
	if err != nil {
		os.Exit(1)
	}
}

func runPrompt() {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print("> ")
		if scanner.Scan() {
			s := scanner.Text()
			if len(s) > 0 {
				// add break line so scanner can add semicolon automatically
				s = fmt.Sprintf("%s\n", scanner.Text())
				run(s)
			}
		} else {
			break
		}
	}
}

func run(code string) (result interface{}, err error) {
	scanner := scan.New(code)
	tokens, err := scanner.Scan() // it doesn't go out of loop here
	if err != nil {
		return
	}
	p := parser.New(tokens)
	exp, err := p.Parse()
	if err != nil {
		return
	}
	// todo: why print statement intepreted even when err
	inter := interp.Interp{}
	result, err = inter.Interpret(exp)
	if result != nil && err == nil {
		// print the end result, exp. good for repl
		fmt.Println(result)
	}
	return
}
