package interp

import (
	"lang/env"
	"lang/expr"
	"time"
)

// can merge into expr
type Interp struct {
}

func (self *Interp) Interpret(stmts []expr.Evaluable) (result interface{}, err error) {
	for _, v := range stmts {
		result, err = v.Evaluate()
		if err != nil {
			return nil, err
		}
	}
	return
}

type Now struct{}

func (Now) Call(args []expr.Evaluable) (result interface{}, err error) {
	return time.Now().UnixNano(), nil
}

func init() {
	// now()
	env.Global.Define("now", Now{})
}
