package parser

import (
	"lang/errs"
	"lang/expr"
	"lang/stmt"
	"lang/token"
)

type Parser struct {
	Tokens  []token.Token
	current int
}

func New(tokens []token.Token) (self *Parser) {
	self = &Parser{}
	self.Tokens = tokens
	return
}

// right now, the whole thing is an expression
func (self *Parser) Parse() (stmts []expr.Evaluable, err error) {
	for !self.isEnd() {
		// err_ to prevent err got overriden
		stat, err_ := self.declaration()
		if err_ != nil {
			err = err_ // make sure if there is err it will persist
			self.synchronize()
			continue
		}
		stmts = append(stmts, stat)
	}
	return
}

// the key is precedence

func (self *Parser) declaration() (result expr.Evaluable, err error) {
	if self.advanceIf(token.FN) {
		return self.fn()
	}
	if self.advanceIf(token.VAR) {
		result, err = self.var_()
		if err != nil {
			return
		}
		// todo: var a, b, c;
		_, err = self.consume(token.SEMICOLON, `Expect ";" after statement.`)
		if err != nil {
			return
		}
		return
	}
	return self.statement()
}

func (self *Parser) fn() (result *stmt.Fn, err error) {
	name, err := self.consume(token.IDENTIFIER, "Expect fn name")
	if err != nil {
		return
	}
	if !self.advanceIf(token.LEFT_PAREN) {
		return nil, errs.New(self.peek(), `Expect '(' after fn declaration name`)
	}
	// exp is variable
	var args []*stmt.Var
	for !self.check(token.RIGHT_PAREN) {
		var arg *stmt.Var
		arg, err = self.var_()
		if err != nil {
			return
		}
		args = append(args, arg)
		if len(args) >= 255 {
			// just report the err, because it's not really an error
			errs.New(self.peek(), "arguments exceeding 255 limit")
		}
		self.advanceIf(token.COMMA)
	}
	_, err = self.consume(token.RIGHT_PAREN, `Expect ')' after arguments`)
	if err != nil {
		return
	}
	var body *stmt.Block
	body, err = self.block()
	if err != nil {
		return
	}
	return &stmt.Fn{name, args, body, nil}, nil
}

func (self *Parser) var_() (result *stmt.Var, err error) {
	name, err := self.consume(token.IDENTIFIER, "Expect variable name")
	if err != nil {
		return
	}
	var init expr.Evaluable
	if self.advanceIf(token.EQUAL) {
		init, err = self.expression()
		if err != nil {
			return
		}
	}
	return stmt.NewVar(name, init), nil
}

func (self *Parser) statement() (result expr.Evaluable, err error) {
	if self.check(token.IF) {
		return self.if_()
	}
	if self.advanceIf(token.WHILE) {
		return self.while()
	}
	if self.advanceIf(token.FOR) {
		return self.for_()
	}
	if self.check(token.LEFT_BRACE) {
		return self.block()
	}
	// PRINT is added as identifier
	isPrint := self.advanceIf(token.PRINT) // important to consume print first
	isReturn := self.advanceIf(token.RETURN)
	exp, err := self.expression()
	if err != nil {
		return nil, err
	}
	_, err = self.consume(token.SEMICOLON, `Expect ";" after statement.`)
	if err != nil {
		return nil, err
	}
	switch true {
	case isPrint:
		result = stmt.NewPrint(exp)
	case isReturn:
		result = stmt.Return{exp}
	default:
		result = stmt.NewExpression(exp)
	}
	return
}

// while true {print 1}
func (self *Parser) while() (result expr.Evaluable, err error) {
	cond, err := self.expression()
	if err != nil {
		return
	}
	body, err := self.block()
	if err != nil {
		return
	}
	return &stmt.While{cond, body}, nil
}

// for var a=1; a <10; a=a+1 {print a}
func (self *Parser) for_() (result expr.Evaluable, err error) {
	var init, cond, change, body expr.Evaluable
	if !self.advanceIf(token.SEMICOLON) { // when init is omited
		init, err = self.declaration()
		if err != nil {
			return
		}
	}
	if !self.advanceIf(token.SEMICOLON) { // when cond is omited
		cond, err = self.statement()
		if err != nil {
			return
		}
	}
	if !self.check(token.LEFT_BRACE) {
		change, err = self.expression()
		if err != nil {
			return
		}
	}
	// ---using existing tool
	body, err = self.block()
	if err != nil {
		return
	}
	stmts := body.(*stmt.Block).Stmts
	if change != nil {
		stmts = append(stmts, change)
	}
	if cond == nil {
		cond = expr.Literal{true}
	}
	body = &stmt.While{cond, &stmt.Block{Stmts: stmts}}
	if init != nil {
		body = &stmt.Block{Stmts: []expr.Evaluable{init, body}}
	}
	return body, nil
	// ---end using existing tool
	//return &stmt.For{init, cond, change, body}, nil
}

// if false ... else if ... else ...
// var a = 2; if a == 0 {print 0} else if a == 1 {print 1} else {print "other"}
func (self *Parser) if_() (expr.Evaluable, error) {
	if !self.advanceIf(token.IF) {
		return nil, errs.New(self.peek(), `Expect if statement`)
	}
	condition, err := self.expression()
	if err != nil {
		return nil, err
	}
	// use statement to omit {}, else use block()
	then, err := self.block() // then is always a block
	if err != nil {
		return nil, err
	}

	var else_ expr.Evaluable
	if self.advanceIf(token.ELSE) {
		if self.check(token.IF) { // else if
			else_, err = self.if_()
			if err != nil {
				return nil, err
			}
		} else {
			else_, err = self.block()
			if err != nil {
				return nil, err
			}
		}
	}
	return stmt.NewIf(condition, then, else_), nil
}

func (self *Parser) block() (*stmt.Block, error) {
	// turn this to non-err to support optional {}
	if !self.advanceIf(token.LEFT_BRACE) {
		return nil, errs.New(self.peek(), "Expect '{' before block")
	}
	var result []expr.Evaluable
	for !self.check(token.RIGHT_BRACE) && !self.isEnd() {
		st, err := self.declaration()
		if err != nil {
			return nil, err
		}
		result = append(result, st)
	}
	// at end and no right brace
	_, err := self.consume(token.RIGHT_BRACE, "Expect '}' after block")
	if err != nil {
		return nil, err
	}
	return stmt.NewBlock(result), nil
}

func (self *Parser) expression() (expr.Evaluable, error) {
	return self.assignment()
}

func (self *Parser) assignment() (expr.Evaluable, error) {
	left, err := self.or()
	if err != nil {
		return nil, err
	}
	// right associative recursion
	if self.advanceIf(token.EQUAL) {
		eq := self.prev()
		// assignment tree nodes instead recur tree nodes
		right, err_ := self.assignment()
		if err_ != nil {
			return nil, err_
		}
		// make sure valid lvalue, no need look ahead or backtracking
		if v, ok := left.(expr.Variable); ok {
			return expr.Assignment{Name: v.Name, Value: right}, nil
		}
		return nil, errs.New(eq, "Invalid assignment target")
	}
	return left, nil
}

func (self *Parser) or() (expr.Evaluable, error) {
	return self.binaryExpr(self.and, token.OR)
}
func (self *Parser) and() (expr.Evaluable, error) {
	return self.binaryExpr(self.equality, token.AND)
}

func (self *Parser) equality() (expr.Evaluable, error) {
	return self.binaryExpr(self.comparison, token.NOT_EQUAL, token.EQUAL_EQUAL)
}

func (self *Parser) comparison() (expr.Evaluable, error) {
	return self.binaryExpr(self.addition, token.GREATER, token.GREATER_EQUAL, token.LESS, token.LESS_EQUAL)
}

func (self *Parser) addition() (expr.Evaluable, error) {
	return self.binaryExpr(self.factor, token.PLUS, token.MINUS)
}

func (self *Parser) factor() (expr.Evaluable, error) {
	return self.binaryExpr(self.unary, token.SLASH, token.STAR)
}

func (self *Parser) unary() (expr.Evaluable, error) {
	// right associative recursive
	if self.advanceIf(token.NOT, token.MINUS) {
		operator := self.prev()
		right, err := self.unary()
		if err != nil {
			return nil, err
		}
		return expr.Unary{operator, right}, nil
	}
	return self.call()
}

func (self *Parser) call() (exp expr.Evaluable, err error) {
	exp, err = self.primary()
	if err != nil {
		return
	}
	for self.advanceIf(token.LEFT_PAREN) { // closure()()
		// exp is variable
		var args []expr.Evaluable
		for !self.check(token.RIGHT_PAREN) {
			var arg expr.Evaluable
			arg, err = self.expression()
			if err != nil {
				return
			}
			args = append(args, arg)
			if len(args) >= 255 {
				// just report the err, because it's not really an error
				errs.New(self.peek(), "arguments exceeding 255 limit")
			}
			self.advanceIf(token.COMMA)
		}
		exp = expr.Call{exp, self.peek(), args}
		_, err = self.consume(token.RIGHT_PAREN, `Expect ')' after arguments`)
	}
	return
}

// highest precedence
func (self *Parser) primary() (exp expr.Evaluable, err error) {
	if self.advanceIf(token.FALSE) {
		return expr.Literal{false}, nil
	}
	if self.advanceIf(token.TRUE) {
		return expr.Literal{true}, nil
	}
	if self.advanceIf(token.NIL) {
		return expr.Literal{nil}, nil
	}
	if self.advanceIf(token.NUMBER, token.STRING) {
		return expr.Literal{self.prev().Literal}, nil
	}
	// why group is in primary: it has the highest precedence
	if self.advanceIf(token.LEFT_PAREN) {
		exp, err := self.expression()
		if err != nil {
			return nil, err
		}
		_, err = self.consume(token.RIGHT_PAREN, `Expect ")" after expression`)
		if err != nil {
			return nil, err
		}
		return expr.Group{exp}, nil
	}
	// usually for var assignment
	if self.advanceIf(token.IDENTIFIER) {
		return expr.Variable{Name: self.prev()}, nil
	}
	// ignore EOF
	if self.advanceIf(token.EOF) {
		return expr.Literal{Value: ""}, nil
	}
	// handleErr needs current token info, so put in current err fn
	//return nil, self.handleErr(self.peek(), "Unexpectd expression.")
	err = errs.New(self.peek(), "Unexpected expression.")
	return
}

func (self *Parser) consume(t token.Type, msg string) (tk token.Token, err error) {
	if self.check(t) {
		tk = self.peek()
		self.advance()
		return
	}
	//err = self.handleErr(self.peek(), msg)
	err = errs.New(self.peek(), msg)
	return
}

type parseFn func() (expr.Evaluable, error)

// parseFn is the main recursion
func (self *Parser) binaryExpr(parse parseFn, types ...token.Type) (expr.Evaluable, error) {
	left, err := parse() // left side may have higher precedence
	if err != nil {
		return nil, err
	}
	// left associative recursive
	for self.advanceIf(types...) {
		op := self.prev()
		right, err := parse()
		if err != nil {
			return nil, err
		}
		left = expr.Binary{left, op, right}
	}
	return left, nil
}

func (self *Parser) advanceIf(types ...token.Type) bool {
	for _, v := range types {
		if self.check(v) {
			self.advance()
			return true
		}
	}
	return false
}

func (self *Parser) check(t token.Type) (ok bool) {
	return self.peek().Type == t
}

// return current token and advance
func (self *Parser) advance() {
	self.current++
}

func (self *Parser) isEnd() bool {
	return self.peek().Type == token.EOF
}

func (self *Parser) Get(index int) token.Token {
	if index >= len(self.Tokens) {
		return token.Empty
	}
	return self.Tokens[index]
}

// check if end before peek
func (self *Parser) peek() token.Token {
	return self.Get(self.current)
}

func (self *Parser) prev() token.Token {
	return self.Get(self.current - 1)
}

// when encounter, to keep parsing, skip until valid (statement starter)
func (self *Parser) synchronize() {
	self.advance()      // skip the erred token
	for !self.isEnd() { // skip until next statement
		if self.prev().Type == token.SEMICOLON {
			return
		}
		switch self.peek().Type {
		case token.CLASS, token.FN, token.VAR, token.FOR, token.IF, token.WHILE, token.PRINT, token.RETURN:
			return
		}
		self.advance()
	}
}

// report and syncronize
// parser error should stay in parser
func (self *Parser) handleErr(tk token.Token, msg string) (err error) {
	self.synchronize()
	return errs.New(tk, msg)
}
