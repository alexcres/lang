package scan

import "lang/token"

var reserved = map[string]token.Type{
	"and":    token.AND,
	"class":  token.CLASS,
	"else":   token.ELSE,
	"false":  token.FALSE,
	"for":    token.FOR,
	"fn":     token.FN,
	"if":     token.IF,
	"nil":    token.NIL,
	"or":     token.OR,
	"print":  token.PRINT,
	"return": token.RETURN,
	"super":  token.SUPER,
	"self":   token.SELF,
	"true":   token.TRUE,
	"var":    token.VAR,
	"while":  token.WHILE,
}
