package token

type Type int

// seperate types for: literal, keywords...
// why not group them into more types?
const (
	// Single-character tokens.
	LEFT_PAREN Type = iota
	RIGHT_PAREN
	LEFT_BRACE
	RIGHT_BRACE
	COMMA
	DOT
	MINUS
	PLUS
	SEMICOLON
	SLASH
	STAR
	// One or two character tokens.
	NOT
	NOT_EQUAL
	EQUAL
	EQUAL_EQUAL
	GREATER
	GREATER_EQUAL
	LESS
	LESS_EQUAL

	// Literals.
	STRING // var
	NUMBER // var
	NIL
	TRUE
	FALSE
	// Keywords.
	IDENTIFIER // var
	AND
	CLASS
	ELSE
	FN
	FOR
	IF
	OR
	PRINT
	RETURN
	SUPER
	SELF
	VAR
	WHILE

	EOF
)

type Literal int

type Keyword struct {
}

var If = Keyword{}

type Operation int
type Punctuation int
