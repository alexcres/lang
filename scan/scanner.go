package scan

import (
	"fmt"
	"lang/errs"
	"lang/token"
	"strconv"
)

type Scanner struct {
	Source               string
	Tokens               []token.Token
	start, current, line int
}

func New(src string) *Scanner {
	var self Scanner
	self.Source = src
	self.line = 1
	return &self
}

var end = false

func (self *Scanner) isEnd() bool {
	return self.current >= len(self.Source)
}

func (self *Scanner) Scan() (tks []token.Token, err error) {
	for !self.isEnd() {
		self.start = self.current
		err = self.ScanOne()
		if err != nil {
			return
		}
	}
	self.Tokens = append(self.Tokens, token.New(token.EOF, "", nil, self.line))
	return self.Tokens, nil
}

// what's a literal
func (self *Scanner) ScanOne() (err error) {
	c := self.Source[self.current]
	// very important advance before switch
	// c already hold the current char
	// advance so in switch can peek next char
	self.advance()
	switch c {
	case ' ', '\r', '\t': // ignore
		return
	case '\n':
		self.line++
		// add semicolon token to allow user omit semicolon
		switch self.peekToken().Type {
		case token.LEFT_BRACE, token.RIGHT_BRACE:
			return
		}
		self.autoSemicolon()
	case '(':
		self.addToken(token.LEFT_PAREN, nil)
	case ')':
		self.addToken(token.RIGHT_PAREN, nil)
	case '{':
		self.addToken(token.LEFT_BRACE, nil)
	case '}':
		// allow last statement and end block on the same line
		self.autoSemicolon()
		self.addToken(token.RIGHT_BRACE, nil)
	case ',':
		self.addToken(token.COMMA, nil)
	case '.':
		self.addToken(token.DOT, nil)
	case '-':
		self.addToken(token.MINUS, nil)
	case '+':
		self.addToken(token.PLUS, nil)
	case ';':
		self.addToken(token.SEMICOLON, nil)
	case '*':
		self.addToken(token.STAR, nil)
	case '!':
		if self.advanceIf('=') {
			self.addToken(token.NOT_EQUAL, nil)
		} else {
			self.addToken(token.NOT, nil)
		}
	case '=':
		if self.advanceIf('=') {
			self.addToken(token.EQUAL_EQUAL, nil)
		} else {
			self.addToken(token.EQUAL, nil)
		}
	case '<':
		if self.advanceIf('=') {
			self.addToken(token.LESS_EQUAL, nil)
		} else {
			self.addToken(token.LESS, nil)
		}
	case '>':
		if self.advanceIf('=') {
			self.addToken(token.GREATER_EQUAL, nil)
		} else {
			self.addToken(token.GREATER, nil)
		}
	case '/':
		if self.advanceIf('/') { // comment
			for self.peek() != '\n' && !self.isEnd() {
				self.advance()
			}
		} else {
			self.addToken(token.SLASH, nil)
		}
	case '"':
		err = self.scanString()
	default:
		if isDigit(c) {
			err = self.scanNumber()
		} else if isAlpha(c) {
			err = self.scanIdentifier()
		} else {
			fmt.Println(string(c))
			return errs.New(self.peekToken(), "Unexpected character.")
		}
	}
	return
}

func (self *Scanner) autoSemicolon() {
	if self.peekToken().Type == token.SEMICOLON {
		return
	}
	self.addToken(token.SEMICOLON, nil)
}

func (self *Scanner) GetToken(index int) token.Token {
	return self.Tokens[index]
}

func (self *Scanner) peekToken() token.Token {
	if len(self.Tokens) == 0 {
		return token.Empty
	}
	return self.Tokens[len(self.Tokens)-1] //current start with 1
}

func (self *Scanner) addToken(_type token.Type, literal interface{}) {
	text := self.Source[self.start:self.current]
	self.Tokens = append(self.Tokens, token.New(_type, text, literal, self.line))
}

func (self *Scanner) scanNumber() error {
	for isDigit(self.peek()) {
		self.advance()
	}
	// decimal part
	if self.peek() == '.' && isDigit(self.peekNext()) {
		self.advance() // consume dot
		for isDigit(self.peek()) {
			self.advance()
		}
	}
	// make all number as float64
	n, err := strconv.ParseFloat(self.Source[self.start:self.current], 64)
	if err != nil {
		return err
	}
	self.addToken(token.NUMBER, n)
	return nil
}

func (self *Scanner) scanString() (err error) {
	for self.peek() != '"' && !self.isEnd() {
		self.advance()
	}
	if self.peek() != '"' {
		return errs.New(token.Token{Lexeme: self.Source[self.start+1 : self.current-1], Line: self.line}, "Undetermined string.")
	}
	// pointer is at the closing quote
	self.advance() // pointer pass the closing string quote
	self.addToken(token.STRING, self.Source[self.start+1:self.current-1])
	return nil
}

// if match, then advance scanner pointer
func (self *Scanner) advanceIf(expected byte) (ok bool) {
	if self.peek() != expected {
		return
	}
	self.advance()
	return true
}

func (self *Scanner) Get(index int) byte {
	// byte is the same as unit8, default empty value to 0
	if index >= len(self.Source) {
		return 0
	}
	return self.Source[index]
}

func (self *Scanner) peek() byte {
	return self.Get(self.current)
}

func (self *Scanner) peekPrev() byte {
	return self.Get(self.current - 1)
}

func (self *Scanner) peekNext() byte {
	return self.Get(self.current + 1)
}

func (self *Scanner) scanIdentifier() error {
	for isAlphaNumeric(self.peek()) {
		self.advance()
	}
	text := self.Source[self.start:self.current]
	if keyword, ok := reserved[text]; ok {
		self.addToken(keyword, nil)
	} else { // todo: text may not needed
		self.addToken(token.IDENTIFIER, text)
	}
	return nil
}

func (self *Scanner) advance() {
	self.current++
}

func isDigit(c byte) bool {
	return c >= '0' && c <= '9'
}

func isAlpha(c byte) bool {
	return (c >= 'a' && c <= 'z') ||
		(c >= 'A' && c <= 'Z') ||
		c == '_'
}

func isAlphaNumeric(c byte) bool {
	return isDigit(c) || isAlpha(c)
}
