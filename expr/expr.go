package expr

import (
	"lang/env"
	"lang/errs"
	"lang/token"
)

// maybe using interface, expr has certain behavior
// interface + composition
// standalone type + type switch?

// contain all props, dynamic check field to determine different type?
type Evaluable interface {
	Evaluate() (interface{}, error)
}

type Callable interface {
	Call(args []Evaluable) (result interface{}, err error)
}

type Call struct {
	Callee Evaluable   // a fn's name
	Pos    token.Token // for call in source code position
	Args   []Evaluable
}

func (self Call) Evaluate() (result interface{}, err error) {
	caller, err := self.Callee.Evaluate() // get fn, ref: variable.evaluate
	if err != nil {
		return
	}
	fn, ok := caller.(Callable)
	if !ok {
		return nil, errs.New(self.Pos, "fn is not a callable")
	}
	return fn.Call(self.Args)
}

type Assignment struct {
	Name  token.Token
	Value Evaluable
}

func (self Assignment) Evaluate() (result interface{}, err error) {
	result, err = self.Value.Evaluate()
	if err != nil {
		return
	}
	env.Current.Assign(self.Name, result)
	return
}

// a primary in an expression
// it's like binary with equal sign as operator
// not merge into binary catches = instead == error
// differ to var: var is an statement start with the 'var' as keyword
type Variable struct {
	Name token.Token
}

func (self Variable) Evaluate() (result interface{}, err error) {
	return env.Current.Get(self.Name)
}

type Group struct {
	Value Evaluable
}

func (self Group) Evaluate() (interface{}, error) {
	return self.Value.Evaluate()
}

type Literal struct {
	Value interface{}
}

func (self Literal) Evaluate() (interface{}, error) {
	return self.Value, nil
}

type Unary struct {
	Operator token.Token
	Right    Evaluable
}

func (self Unary) Evaluate() (result interface{}, err error) {
	right, err := self.Right.Evaluate()
	if err != nil {
		return
	}
	switch self.Operator.Type {
	case token.MINUS:
		err = checkNumber(self.Operator, right)
		if err != nil {
			return
		}
		v := right.(float64) // controlled by scanNumber, which conform all num to float64
		result = -v
	case token.NOT:
		result = !IsTrue(right)
	}
	return
}

func IsTrue(obj interface{}) bool {
	if obj == nil {
		return false
	}
	switch v := obj.(type) {
	case bool:
		return v
	case string:
		return v != ""
	case float64:
		return v != 0
	}
	return true
}

type Binary struct {
	Left     Evaluable
	Operator token.Token
	Right    Evaluable
}

func NewBinary(left Evaluable, operator token.Token, right Evaluable) Binary {
	return Binary{left, operator, right}
}

func (self Binary) Evaluate() (result interface{}, err error) {
	left, err := self.Left.Evaluate()
	if err != nil {
		return
	}
	switch self.Operator.Type {
	// short circuit for "and", "or"
	case token.OR:
		// print "hi" or 1 should output hi instead of true
		if IsTrue(left) {
			return left, nil
		}
	case token.AND:
		if !IsTrue(left) {
			return left, nil
		}
	}
	right, err := self.Right.Evaluate()
	if err != nil {
		return
	}
	switch self.Operator.Type {
	case token.OR, token.AND:
		return right, nil
	case token.MINUS:
		err = checkNumber(self.Operator, left, right)
		if err != nil {
			return
		}
		return left.(float64) - right.(float64), nil
	case token.SLASH:
		err = checkNumber(self.Operator, left, right)
		if err != nil {
			return
		}
		return left.(float64) / right.(float64), nil
	case token.STAR:
		err = checkNumber(self.Operator, left, right)
		if err != nil {
			return
		}
		return left.(float64) * right.(float64), nil
	case token.PLUS:
		n1, ok1 := left.(float64)
		n2, ok2 := right.(float64)
		if ok1 && ok2 {
			return n1 + n2, nil
		}
		s1, ok1 := left.(string)
		s2, ok2 := right.(string)
		if ok1 && ok2 {
			return s1 + s2, nil
		}
		return nil, errs.New(self.Operator, "Operands must be numbers or strings")
	case token.GREATER:
		err = checkNumber(self.Operator, left, right)
		if err != nil {
			return
		}
		return left.(float64) > right.(float64), nil
	case token.GREATER_EQUAL:
		err = checkNumber(self.Operator, left, right)
		if err != nil {
			return
		}
		return left.(float64) >= right.(float64), nil
	case token.LESS:
		err = checkNumber(self.Operator, left, right)
		if err != nil {
			return
		}
		return left.(float64) < right.(float64), nil
	case token.LESS_EQUAL:
		err = checkNumber(self.Operator, left, right)
		if err != nil {
			return
		}
		return left.(float64) <= right.(float64), nil
	case token.NOT_EQUAL:
		err = checkNumber(self.Operator, left, right)
		if err != nil {
			return
		}
		return !isEqual(left, right), nil
	case token.EQUAL_EQUAL:
		err = checkNumber(self.Operator, left, right)
		if err != nil {
			return
		}
		return isEqual(left, right), nil
	}
	return
}

func isEqual(a interface{}, b interface{}) (result bool) {
	if a == nil && b == nil {
		return true
	}
	if a == nil || b == nil {
		return false
	}
	// string, num, bool
	switch v := a.(type) {
	case string:
		s, ok := b.(string)
		if !ok {
			return
		}
		return v == s
	case float64:
		n, ok := b.(float64)
		if !ok {
			return
		}
		return v == n
	case bool:
		bo, ok := b.(bool)
		if !ok {
			return
		}
		return v == bo
	}
	return false
}

var literal = map[int]token.Type{}
var Exprs = map[int]interface{}{}

func checkNumber(op token.Token, operand ...interface{}) (err error) {
	for _, v := range operand {
		_, ok := v.(float64)
		if !ok {
			return errs.New(op, "Operand must be a number")
		}
	}
	return
}
