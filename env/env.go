package env

import (
	"fmt"
	"lang/errs"
	"lang/token"
)

type Environment struct {
	outer   *Environment
	Current map[string]interface{}
}

func NewGlobal() (global *Environment) {
	self := Environment{}
	self.Current = make(map[string]interface{})
	return &self
}

func NewInner(outer *Environment) (inner *Environment) {
	self := Environment{}
	self.outer = outer
	self.Current = make(map[string]interface{})
	return &self
}
func Enter() (inner *Environment) {
	Current = NewInner(Current)
	return Current
}
func Exit(here *Environment) {
	Current = here.Outer()
	if Current == nil {
		Current = Global
	}
}

var Global = NewGlobal()
var Current = Global

func (self *Environment) Merge(other *Environment) {
	for k, v := range other.Current {
		self.Current[k] = v
	}
}

func (self *Environment) Copy() *Environment {
	new_env := Environment{}
	new_env.outer = self.outer
	new_env.Current = make(map[string]interface{})
	for k, v := range self.Current {
		switch value := v.(type) {
		default:
			new_env.Current[k] = value
		}
	}
	return &new_env
}

func (self *Environment) Outer() *Environment {
	return self.outer
}

func (self *Environment) Define(name string, value interface{}) {
	self.Current[name] = value
}

func (self *Environment) Get(name token.Token) (interface{}, error) {
	if v, ok := self.Current[name.Lexeme]; ok {
		return v, nil
	}
	if self.outer == nil {
		return nil, errs.New(name, fmt.Sprintf("Undefined variable \"%s\"", name.Lexeme))
	}
	return self.outer.Get(name)
}

func (self *Environment) Assign(name token.Token, value interface{}) (err error) {
	// make sure exist before assign
	envi := self
	_, ok := self.Current[name.Lexeme]
	for !ok {
		if envi.outer == nil {
			return errs.New(name, fmt.Sprintf("Assignment to undefinned variable \"%s\"", name.Lexeme))
		}
		envi = envi.outer
		_, ok = envi.Current[name.Lexeme]
	}
	envi.Current[name.Lexeme] = value
	return nil
}
