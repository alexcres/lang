# A Programming Language

- [text] -> scanner -> [token] -> parser -> [evaluable] -> interpreter -> [value] 
- OOP is not implemented(nothing new to learn), otherwise it's fullfeatured

## How to Use

- REPL: `go run main.go`
  - `fn fib(n) {if n <=1 {return n} return fib(n-2)+fib(n-1)} fib(10)` returns 55
- Script: `go run main.go file`

## Grammer

```
// optional semicolon
var variable = value

{block_scope}

fn function(param) {
	statement_1
	statement_2
	return value
}

if condition {
	statements
} else if condition {
	statements
} else {
	statements
}

while condition {
	statements
}

for init; mutation; condition {
	statements
}

// one native function
now() // timestamp in nanoseconds, for benchmark
```

## What's Next?

A language with optional static typing, garbage collection written in `C++`.