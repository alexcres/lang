package errs

import (
	"fmt"
	"lang/token"
)

type Error struct {
	tk  token.Token
	msg string
}

func New(tk token.Token, msg string) *Error {
	self := &Error{tk, msg}
	fmt.Println(self.Error()) // report all errors
	return self
}

func (self *Error) Error() string {
	return fmt.Sprintf(`[line %v] Error %v: %v`, self.tk.Line, self.tk.Lexeme, self.msg)
}

//var HasError = false
//var HasRuntimeError = false

// design: separate err gen and err report
//func Error(line int, message string) string {
//	return Report(line, "", message)
//}

//func Report(line int, where string, message string) (result string) {
//	result = fmt.Sprintf(`[line %v] Error %v: %v`, line, where, message)
//	HasError = true
//	fmt.Println(result)
//	return
//}

//func Runtime(line int, msg string) {
//	HasRuntimeError = true
//	fmt.Printf("%s\n[line %v ]", msg, line)
//}
