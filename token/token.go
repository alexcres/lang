package token

import (
	"fmt"
)

type Token struct {
	Type    Type
	Lexeme  string
	Literal interface{}
	Line    int
}

func New(_type Type, lexeme string, literal interface{}, line int) (self Token) {
	self.Type = _type
	self.Lexeme = lexeme // the original user text
	self.Literal = literal
	self.Line = line
	return
}

func (self Token) String() string {
	return fmt.Sprintf(`%s %s %v`, self.Type, self.Lexeme, self.Literal)
}

var Empty = New(EOF, "", nil, 0)

var Tokens = map[string]Token{
	"(":          Token{},
	")":          Token{},
	"[":          Token{},
	"]":          Token{},
	",":          Token{},
	".":          Token{},
	"-":          Token{},
	"+":          Token{},
	";":          Token{},
	"/":          Token{},
	"*":          Token{},
	"!":          Token{},
	"!=":         Token{},
	"=":          Token{},
	"==":         Token{},
	">":          Token{},
	">=":         Token{},
	"<":          Token{},
	"<=":         Token{},
	"string":     Token{},
	"number":     Token{},
	"nil":        Token{},
	"true":       Token{},
	"false":      Token{},
	"identifier": Token{},
	"and":        Token{},
	"class":      Token{},
	"else":       Token{},
	"fn":         Token{},
	"for":        Token{},
	"if":         Token{},
	"or":         Token{},
	"print":      Token{},
	"return":     Token{},
	"super":      Token{},
	"self":       Token{},
	"var":        Token{},
	"while":      Token{},
	"eof":        Token{},
}
