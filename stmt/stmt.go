package stmt

import (
	"fmt"
	"lang/env"
	"lang/errs"
	"lang/expr"
	"lang/token"
)

// todo: change Evaluate to Visit? and wrap expr in stmt
// convert expression to statement
type Stmt struct {
	expression expr.Evaluable
}

func (self *Stmt) Evaluate() (result interface{}, err error) {
	return self.expression.Evaluate()
}

type ReturnExcept struct {
	Value interface{}
}

func (self ReturnExcept) Error() string {
	return "Return can only exist in function definition"
}

type Block struct {
	Env   *env.Environment
	Stmts []expr.Evaluable
}

func NewBlock(stmts []expr.Evaluable) *Block {
	self := Block{}
	self.Stmts = stmts
	return &self
}

func (self *Block) Evaluate() (result interface{}, err error) {
	if self.Env == nil {
		self.Env = env.Enter()
	}
	defer env.Exit(self.Env)
	for _, v := range self.Stmts {
		//fmt.Println(len(self.Env.Current))
		//for k, v := range self.Env.Current {
		//	fmt.Println(k, v)
		//}
		if re, ok := v.(Return); ok {
			return re.Evaluate()
		}
		result, err = v.Evaluate()
		if err != nil {
			switch err.(type) {
			case ReturnExcept:
				return result, err
			default:
				return nil, err
			}
		}
	}
	return nil, nil
}

type Fn struct {
	Name     token.Token
	Param    []*Var
	Body     *Block
	Surround *env.Environment
}

func (self *Fn) String() string {
	return fmt.Sprintf("<fn %s>", self.Name.Lexeme)
}

// callee use fn in interp
// fn count(n) { if n > 1 {return "bigger"} print "Smaller"} count(3)
// fn add(a, b=2) {return a+b} add(1)
func (self Fn) Call(args []expr.Evaluable) (result interface{}, err error) {
	// call should copy the env
	if len(self.Param) < len(args) {
		return nil, errs.New(self.Name, fmt.Sprintf("arguemnts %v exceeding parameters %v", len(args), len(self.Param)))
	}

	env.Enter()
	// for closure
	if self.Surround != nil {
		env.Current.Merge(self.Surround)
	}

	for i, param := range self.Param {
		if i < len(args) {
			// for default value
			param.Expression = args[i] // expression is a pointer, will get overwritten

		}
		_, err = param.Evaluate() // registe param in current env
		if err != nil {
			env.Exit(env.Current)
			return
		}
	}
	self.Body.Env = env.Current
	result, err = self.Body.Evaluate()
	// the current env doesn't exist here

	if err != nil {
		switch err.(type) {
		case ReturnExcept: // more err types to come, checking if result is nil is not enough
			return result, nil
		default:
			return nil, err
		}
	}
	return
}

// turn token to fn in parser
func NewFn(name token.Token, body *Block, args ...*Var) *Fn {
	self := Fn{}
	self.Param = args
	self.Name = name
	self.Body = body
	return &self
}

// regist fn in env in interp
func (self *Fn) Evaluate() (result interface{}, err error) {
	env.Current.Define(self.Name.Lexeme, self) // register fn name
	// fn doesn't need to hold onto surrounding env unless it's return inside fn
	//self.Surround = env.Current                // hold onto surrounding env, for closure
	return
}

// todo: break, continue, may in if block
type While struct {
	Cond expr.Evaluable
	Body *Block
}

func (self *While) Evaluate() (result interface{}, err error) {
	cond := true
	for cond {
		if self.Cond != nil {
			result, err = self.Cond.Evaluate()
			if err != nil {
				return
			}
			cond = expr.IsTrue(result)
		}
		if cond && self.Body != nil {
			result, err = self.Body.Evaluate()
			if err != nil {
				return
			}
		}
	}
	return nil, nil // discard result
}

type For struct {
	Init      expr.Evaluable
	Condition expr.Evaluable
	Change    expr.Evaluable
	Body      *Block
}

func (self *For) Evaluate() (result interface{}, err error) {
	defer env.Exit(env.Enter())
	if self.Init != nil {
		_, err = self.Init.Evaluate()
		if err != nil {
			return
		}
	}
	cond := true

	for cond {
		if self.Condition != nil {
			result, err = self.Condition.Evaluate()
			if err != nil {
				return
			}
			cond = expr.IsTrue(result)
		}
		if cond {
			result, err = self.Body.Evaluate()
			if err != nil {
				return
			}
		} else {
			break
		}
		if self.Change != nil {
			result, err = self.Change.Evaluate()
			if err != nil {
				return
			}
		}
	}
	return nil, nil
}

type If struct {
	Condition expr.Evaluable
	Then      *Block
	Else      expr.Evaluable // can chain if else
}

func NewIf(condition expr.Evaluable, then *Block, else_ expr.Evaluable) *If {
	self := If{condition, then, else_}
	return &self
}

func (self *If) Evaluate() (result interface{}, err error) {
	ok, err := self.Condition.Evaluate()
	if err != nil {
		return
	}
	if expr.IsTrue(ok) {
		result, err = self.Then.Evaluate()
	} else if self.Else != nil {
		result, err = self.Else.Evaluate()
	}
	return
}

// fn closure(n) {
//    fn add(m){return m+n} // need to access n
//    return add
// }
// var a = closure(1)
// var b = closure(2)

type Return struct {
	Expression expr.Evaluable
}

func (self Return) Evaluate() (result interface{}, err error) {
	result, err = self.Expression.Evaluate()
	if err != nil {
		return
	}
	if fn, ok := result.(*Fn); ok { // fn is a pointer, return should copy fn for self-contained env
		// closure only make sense if return fn
		var new_fn = *fn
		new_fn.Surround = env.Current
		result = new_fn
	}
	return result, ReturnExcept{Value: result}
}

type Print struct {
	Stmt
}

func NewPrint(v expr.Evaluable) *Print {
	self := Print{}
	self.expression = v
	return &self
}

func (self *Print) Evaluate() (result interface{}, err error) {
	value, err := self.expression.Evaluate()
	if err != nil {
		return
	}
	fmt.Println(value)
	return
}

// expression statement
type Expression struct {
	Stmt
}

func NewExpression(exp expr.Evaluable) *Expression {
	self := Expression{}
	self.expression = exp
	return &self
}

func (self *Expression) Evaluate() (result interface{}, err error) {
	result, err = self.expression.Evaluate()
	return
}

type Var struct {
	Name       token.Token
	Expression expr.Evaluable
}

func NewVar(name token.Token, init expr.Evaluable) *Var {
	self := Var{}
	self.Name = name
	self.Expression = init
	return &self
}

// more like visit
func (self *Var) Evaluate() (result interface{}, err error) {
	if self.Expression != nil {
		result, err = self.Expression.Evaluate()
		if err != nil {
			return
		}
		env.Current.Define(self.Name.Lexeme, result)
		//fmt.Printf("in env %p, evaluating %s = %v\n", env.Current, self.Name.Lexeme, result)
	}
	return
}
