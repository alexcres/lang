// resolve definition to declaration
// for scoping and optimization
package resolver

import "lang/token"

type Scope map[string]bool
type Stack []Scope

var scope = make(Scope)
var stack = make(Stack, 0)

func BeginScope() {
	stack = append(stack, make(Scope))
}

func EndScope() {
	stack = stack[:len(stack)-1]
}

func Declare(name token.Token) {
	cur := peek()
	if cur == nil {
		return
	}
	cur[name.Lexeme] = false
}

func Define(name token.Token) {
	cur := peek()
	if cur == nil {
		return
	}
	cur[name.Lexeme] = true
}

func IsEmpty() bool {
	return len(stack) == 0
}

func peek() Scope {
	last := len(stack) - 1
	if last < 0 {
		return nil
	}
	return stack[last]
}

type Resolver struct {
}
